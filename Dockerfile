FROM php:8.2-apache
RUN apt-get update  \
    && apt-get upgrade  \
    && apt-get clean  \
    && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install mysqli pdo pdo_mysql
ADD src /var/www/html
EXPOSE 80