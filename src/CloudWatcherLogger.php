<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload`

use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Maxbanton\Cwh\Handler\CloudWatch;
use Monolog\Logger;
use Monolog\Formatter\JsonFormatter;



class CloudWatchLogger
{
    private static $log;

    private static function init()
    {
        $sdkParams = [
            'region' => 'eu-west-3',
            'version' => 'latest',
            'credentials' => [
                'key' => getenv('AWS_ACCESS_KEY_ID'),
                'secret' => getenv('AWS_SECRET_ACCESS_KEY')
            ]
        ];

        $client = new CloudWatchLogsClient($sdkParams);

        $groupName = 'php-logtest-pe';
        $streamName = 'ecs-stream-ynov-pe';
        $retentionDays = 30;

        $handler = new CloudWatch($client, $groupName, $streamName, $retentionDays, 10000, ['my-awesome-tag' => 'tag-value']);


        $handler->setFormatter(new JsonFormatter());

        self::$log = new Logger('name');

        self::$log->pushHandler($handler);
    }



    public static function logError($message)
    {
        if (!self::$log) {
            self::init();
        }
        self::$log->error($message);
    }

    public static function logWarning($message)
    {
        if (!self::$log) {
            self::init();
        }
        self::$log->warning($message);
    }

    public static function logDebug($message)
    {
        if (!self::$log) {
            self::init();
        }
        self::$log->debug($message);
    }
}
