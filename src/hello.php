<?php

require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload
require_once './CloudWatcherLogger.php';

use HelloWorld\SayHello;



CloudWatchLogger::logDebug("[hello.php] Hello.php");
echo SayHello::world();

